defmodule TimexWeb.ClockManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()

    Process.send_after(self(), :working, 1000)
    {:ok, %{ui_pid: ui, mode: Time, time: Time.from_erl!(now), st2: Idle, count: 0, show: true,  st1: Working, selection: nil, te2e: nil, alarma_edit: False, alarma: Time.add(Time.from_erl!(now), 60, :second)}}
  end

  def handle_info(:"bottom-left", %{st2: Idle, mode: Time} = state) do
    Process.send_after(self(), Waiting2Edit, 250)
    {:noreply, %{state | st2: Waiting, alarma_edit: true}}
  end
  def handle_info(:"bottom-left", %{st2: Waiting, mode: Time} = state) do
    {:noreply, %{state | st2: Idle, alarma_edit: false}}
  end

  def handle_info(:"top-left", %{mode: Time} = state) do
    {:noreply, %{state | mode: SWatch}}
  end

  def handle_info(:"top-left", %{mode: SWatch, ui_pid: ui, time: time} = state) do
    GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, %{state | mode: Time}}
  end

  def handle_info(:working, %{ui_pid: ui, mode: mode, time: time, st1: Working, alarma: alarma} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    if mode == alarma do
      :gproc.send({:p, :l, :ui_event}, :start_alarm)
    end
    if mode == Time do
      GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    end
    {:noreply, state |> Map.put(:time, time) }
  end

  def handle_info(:stop_clock, %{st1: Working} = state) do
    {:noreply, %{state | st1: Stopped, mode: TEditing}}
  end

  def handle_info(:resume_clock, %{st1: Stopped} = state) do
    Process.send_after(self(), :working, 1000)
    {:noreply, %{state | st1: Working, mode: Time}}
  end

  def handle_info(:"bottom-right", %{st2: Idle, mode: Time} = state) do
    Process.send_after(self(), Waiting2Editing, 250)
    {:noreply, %{state | st2: Waiting}}
  end

  def handle_info(:"bottom-right", %{st2: Waiting, mode: Time} = state) do

    {:noreply, %{state | st2: Idle}}
  end


  def handle_info(Waiting2Editing, %{st2: Waiting} = state) do
    te2e = Process.send_after(self(), Editing2Editing, 250)
    :gproc.send({:p, :l, :ui_event}, :stop_clock)
    {:noreply, %{state | st2: Editing, count: 0, show: true, selection: Hour, te2e: te2e}}
  end

  def handle_info(Editing2Editing, %{ui_pid: ui, time: time, st2: Editing, count: count, show: show, selection: selection, te2e: te2e, alarma: alarma, alarma_edit: alarma_edit} = state) do
    Process.cancel_timer(te2e)

    if count < 20 do
      count = count + 1
      show = !show
      te2e = Process.send_after(self(), Editing2Editing, 250)
      GenServer.cast(ui, {:set_time_display, format(if alarma_edit do alarma else time end, show, selection)})
      {:noreply, %{state | st2: Editing, count: count, show: show, te2e: te2e}}
    else
      GenServer.cast(ui, {:set_time_display, format(if alarma_edit do alarma else time end, true, selection)})
      :gproc.send({:p, :l, :ui_event}, :resume_clock)
      {:noreply, %{state | st2: Idle, count: count, show: show, te2e: te2e}}
    end
  end
  def handle_info(:"bottom-right", %{ui_pid: ui, st2: Editing, selection: selection, time: time, te2e: te2e, alarma: alarma, alarma_edit: alarma_edit} = state) do
    Process.cancel_timer(te2e)
    te2e = Process.send_after(self(), Editing2Editing, 250)
    selection = case selection do
       Hour -> Minute
       Minute -> Second
       _ -> Hour
    end

    GenServer.cast(ui, {:set_time_display, format(if alarma_edit do alarma else time end, true, selection)})
    {:noreply, %{state | st2: Editing, count: 0, show: true, selection: selection, te2e: te2e}}
  end

  def handle_info(:"bottom-left", %{st2: Editing, time: time, selection: selection, alarma: alarma, alarma_edit: alarma_edit} = state) do
    Process.send_after(self(), :working, 1000)

    if alarma_edit do
      alarma = case selection do
        Hour ->     Time.add(alarma, 3600)
        Minute ->   Time.add(alarma, 60)
        _ -> Time.add(alarma, 1)
      end
      {:noreply, %{state | st2: Editing, selection: selection, time: time, alarma: alarma, alarma_edit: alarma_edit}}
    else
      time = case selection do
        Hour -> Time.add(time, 3600)
        Minute -> Time.add(time, 60)
        _ -> Time.add(time, 1)
      end
      {:noreply, %{state | st2: Editing, selection: selection, time: time, alarma: alarma, alarma_edit: alarma_edit}}
    end
  end

  def handle_info(:working, state), do: {:noreply, state}

  def handle_info(:"bottom-left", state), do: {:noreply, state}

  def handle_info(:"top-right", state), do: {:noreply, state}

  def handle_info(:"top-left", state), do: {:noreply, state}

  def handle_info(:start_alarm, state), do: {:noreply, state}

  def handle_info(:alarm_off, state), do: {:noreply, state}

  def handle_info(:stop_clock, state), do: {:noreply, state}

  def handle_info(:"bottom-right", state), do: {:noreply, state}
  
  def handle_info(:resume_clock, state), do: {:noreply, state}

  def format(time, show, selection) do
      if(show) do
        "#{time.hour}:#{time.minute}:#{time.second}"
      else
        case selection do
          Hour -> "  :#{time.minute}:#{time.second}"
          Minute -> "#{time.hour}:  :#{time.second}"
          _ -> "#{time.hour}:#{time.minute}:  "
        end
      end
  end

end
